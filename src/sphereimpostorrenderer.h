#pragma once
#include "renderer.h"
#include "logging.h"
#include "sphereimpostorvertex.h"
#include <vector>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>

using std::vector;

class SphereImpostorRenderer :  public IndexedRenderer
{
public:
    static constexpr size_t MaxVertices = 65536 / sizeof(SphereImpostorVertex);

    SphereImpostorRenderer(const vector<SphereImpostorVertex>& vertices);
    void addVertices(const vector<SphereImpostorVertex>& vertices);
    virtual ~SphereImpostorRenderer();
    const GroupIndex<SphereImpostorVertex>& group(size_t i) { return m_groups.at(i); }
    void setColor(const GroupIndex<SphereImpostorVertex>& index, const QVector3D& color);
    void toggleSelected(const vector<size_t>& indices);
    inline size_t size() { return m_vertices.size()/4; }
    inline float sphereRadius(size_t idx) const {
        return m_vertices[idx*4].radius();
    }
private:
    void updateBuffers();
    QOpenGLBuffer m_vertex;
    vector<SphereImpostorVertex> m_vertices;
    vector<GLuint> m_indices;
    // TODO convert this from a vector to a map with IDs
    vector<GroupIndex<SphereImpostorVertex>> m_atoms;
    vector<GroupIndex<SphereImpostorVertex>> m_groups;
protected:
    bool m_impostor = true;
    std::shared_ptr<spd::logger> LOG;
};

#pragma once
#include <QVector3D>
#include "element.h"
#include "cylinderimpostorvertex.h"
#include "sphereimpostorvertex.h"




class Atom
{
private:
    QVector3D m_position = {0.0f, 0.0f, 0.0f};
    Element m_element = HYDROGEN;
public:
    Atom(QVector3D position, Element element) : m_position{position}, m_element{element} {}
    Atom() : m_position{QVector3D(0.0f, 0.0f, 0.0f)}, m_element(HYDROGEN) {}
    Element element() const { return m_element; }
    QVector3D position() const { return m_position;}
    QVector3D color() const { return m_element.color(); }
    bool isBondedTo(const Atom& atom) const;
    vector<SphereImpostorVertex> asSphereImpostorVertices() const;
};

class Bond
{
public:
    std::size_t from = 0;
    std::size_t to = 0;
};

class Fragment
{
private:
    vector<Atom> m_atoms;
    vector<Bond> m_bonds;
public:
    vector<CylinderImpostorVertex> bondCylinders();
    vector<SphereImpostorVertex> atomSpheres();
    vector<CylinderImpostorVertex> cylinderForBond(const Bond& bond);
    Fragment(const vector<Atom>& atoms, const vector<Bond>& bonds) : m_atoms(atoms), m_bonds(bonds) {}
};

#include "atom.h"

vector<SphereImpostorVertex> Atom::asSphereImpostorVertices() const {
    const float scale_factor = 1.0f;
    return {
        SphereImpostorVertex(m_position, color(),
                             m_element.atomic_radius * scale_factor, {-1, 1}),
        SphereImpostorVertex(m_position, color(),
                             m_element.atomic_radius * scale_factor, {-1, -1}),
        SphereImpostorVertex(m_position, color(),
                             m_element.atomic_radius * scale_factor, {1, 1}),
        SphereImpostorVertex(m_position, color(),
                             m_element.atomic_radius * scale_factor, {1, -1}),
    };
}

bool Atom::isBondedTo(const Atom& atom) const {
    float distance = m_position.distanceToPoint(atom.position());
    return (distance < (m_element.atomic_radius  + atom.element().atomic_radius ) * 1.5f);
}


vector<SphereImpostorVertex> Fragment::atomSpheres() {
    vector<SphereImpostorVertex> spheres;
    for(const Atom& a: m_atoms) {
        auto verts = a.asSphereImpostorVertices();
        spheres.insert(spheres.end(), verts.begin(), verts.end());
    }
    return spheres;

}

vector<CylinderImpostorVertex> Fragment::bondCylinders() {
    vector<CylinderImpostorVertex> cylinders;
    for(auto bond: m_bonds) {
        auto verts = cylinderForBond(bond);
        cylinders.insert(cylinders.end(), verts.begin(), verts.end());
    }
    return cylinders;
}


vector<CylinderImpostorVertex> Fragment::cylinderForBond(const Bond& bond) {
        const Atom& a = m_atoms[bond.from];
        const Atom& b = m_atoms[bond.to];
        QVector3D color_a = a.color();
        QVector3D color_b = b.color();

        QVector3D midpoint = 0.5 * (a.position() + b.position());
        color_a.setZ(color_a.z() == 0.0f ? -0.0001f : -color_a.z());
        color_b.setZ(color_b.z() == 0.0f ? -0.0001f : -color_b.z());
        const float radius = 0.23f * 0.9f;
        return {
            CylinderImpostorVertex(a.position(), midpoint, color_a, -radius),
            CylinderImpostorVertex(a.position(), midpoint, color_a, radius),
            CylinderImpostorVertex(a.position(), midpoint, a.color(), -radius),
            CylinderImpostorVertex(a.position(), midpoint, a.color(), radius),
            CylinderImpostorVertex(b.position(), midpoint, color_b, -radius),
            CylinderImpostorVertex(b.position(), midpoint, color_b, radius),
            CylinderImpostorVertex(b.position(), midpoint, b.color(), -radius),
            CylinderImpostorVertex(b.position(), midpoint, b.color(), radius)
        };
}

#include "datastore.h"
#include "logging.h"
#include <QSqlError>
#include <QHostInfo>

Datastore::Datastore(const QString& path)
{
    LOG = getLogger("files");
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(path);
    if (!m_db.open())
    {
        LOG->error("Error: connection with database failed");
    }
    else
    {
        LOG->info("Database: ok");
        createTables();
        startSession();
    }
}

Datastore::~Datastore()
{
    if (m_db.isOpen())
    {
        m_db.close();
    }
}

bool Datastore::createTables()
{
    bool success = true;
    if(!m_db.tables().contains("jobs")) {
        QSqlQuery query;

        success = query.exec("CREATE TABLE jobs ( "
                             "job_id TEXT PRIMARY KEY, "
                             "kind TEXT, "
                             "timestamp DATETIME DEFAULT CURRENT_TIMESTAMP "
                             ");");
        if(!success) {
            LOG->error(ll("failed creating table 'jobs': " +
                          query.lastError().text()));
        }
    }

    if(!m_db.tables().contains("jobfiles")) {
        QSqlQuery query;

        success = query.exec(
                    "CREATE TABLE jobfiles ( "
                    "job_id, "
                    "filename TEXT NOT NULL, "
                    "contents BLOB, "
                    "kind TEXT CHECK( kind IN ('I','O', 'W', 'M') ) "
                    "NOT NULL DEFAULT 'M', "
                    "timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, "
                    "PRIMARY KEY ( job_id, filename ), "
                    "FOREIGN KEY(job_id) REFERENCES job(job_id)"
                    ");"
                    );

        if(!success) {
            LOG->error(ll("failed creating table 'jobfiles': " +
                           query.lastError().text()));
        }
    }

    if(!m_db.tables().contains("sessions")) {
        QSqlQuery query;

        success = query.exec(
                    "CREATE TABLE sessions ( "
                    "host TEXT NOT NULL, "
                    "timestamp DATETIME DEFAULT CURRENT_TIMESTAMP, "
                    "PRIMARY KEY ( host, timestamp ) "
                    ");"
                    );

        if(!success) {
            LOG->error(ll("failed creating table 'sessions': " +
                           query.lastError().text()));
        }
    }

    return success;
}

bool Datastore::startSession() {
    QSqlQuery query;

    QString host = QHostInfo::localHostName();
    LOG->debug(ll("Starting session @" + host));

    query.prepare("INSERT INTO sessions (host) VALUES (:host)");
    query.bindValue(":host", host);
    bool success = query.exec();

    if(!success) {
        LOG->error(ll("failed querying table 'sessions': " +
                       query.lastError().text()));
        return success;
    }
    return success;
}

bool Datastore::addJobFile(const QString& job_id, const TaskFile& file)
{
    bool success = false;
    QSqlQuery query;
    query.prepare("INSERT INTO jobfiles (job_id, filename, contents, kind)"
                  " VALUES (:id, :filename, :contents, :kind)");
    query.bindValue(":id", job_id);
    query.bindValue(":filename", file.filename());
    query.bindValue(":contents", file.contents());
    query.bindValue(":kind", file.kindString());
    success = query.exec();
    if(!success) {
        LOG->error(ll("Couldn't execute query: " +
                      query.lastError().text()));
        LOG->error(ll("Query text: " +
                      query.executedQuery()));
    }
    return success;
}


bool Datastore::addJob(const QString& job_id, const QString& kind)
{
    bool success = false;
    QSqlQuery query;
    query.prepare("INSERT INTO jobs (job_id, kind)"
                  " VALUES (:id, :kind)");
    query.bindValue(":id", job_id);
    query.bindValue(":kind", kind);
    success = query.exec();
    if(!success) {
        LOG->error(ll("Couldn't execute query: " +
                      query.lastError().text()));
        LOG->error(ll("Query text: " +
                      query.executedQuery()));
    }
    return success;
}


QByteArray Datastore::retrieveJobFile(const QString& job_id,
                                      const QString& filename) const
{
    bool success = false;
    QSqlQuery query;

    query.prepare("SELECT contents FROM jobfiles WHERE "
                  "job_id == :job_id AND filename == :filename LIMIT 1;");
    query.bindValue(":job_id", job_id);
    query.bindValue(":filename", filename);
    success = query.exec();
    if(!success) {
        LOG->error(ll("Couldn't execute query: " +
                      query.lastError().text()));
        LOG->error(ll("Query text: " +
                      query.executedQuery()));
    }
    return query.value(0).toByteArray();
}

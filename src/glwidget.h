#pragma once
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLFramebufferObject>
#include <QOpenGLVertexArrayObject>
#include <QMatrix4x4>
#include <QTime>
#include "scene.h"
#include "logging.h"


class QOpenGLShaderProgram;
// Other includes
class QOpenGLDebugMessage;
class QOpenGLDebugLogger;

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    GLWidget(QWidget* parent = 0 );
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void setScene(Scene * scene);
    Scene * scene() const { return m_scene; }

signals:
    void fpsUpdate(float fps);

protected slots:
    void teardownGL();
    void update();
    void messageLogged(const QOpenGLDebugMessage &msg);

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    QOpenGLDebugLogger *m_debugLogger;
    bool m_leftButtonPressed;
    QPoint m_prevPos;
    QPoint m_pos;
    QTime m_time;
    Scene * m_scene;
    int m_frameCount;
    const static int FRAME_UPDATE_INTERVAL = 30;
    float m_fps;
    // Private Helpers
    void printVersionInformation();
    Logger m_log;
};

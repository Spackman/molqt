#include "meshrenderer.h"
#include "sbfmeshwrapper.h"
#include <QOpenGLShaderProgram>
#include <QMatrix3x3>
#include <QFileInfo>

MeshRenderer::MeshRenderer(const vector<MeshVertex>& vertices,
                           const vector<GLuint>& indices) :
    m_vertex(QOpenGLBuffer::VertexBuffer)
{
    m_log = getLogger("graphics");
    m_log->debug("Constructing mesh from verts and indices");
    init(vertices, indices);
    m_id = "MeshRenderer:" + Renderer::generateId();
}

MeshRenderer::MeshRenderer(const QString& filename) :
    m_vertex(QOpenGLBuffer::VertexBuffer)
{
    m_log = getLogger("graphics");
    m_log->debug("Constructing mesh from sbf file");
    auto mesh = fromSBF(filename);
    auto basename = QFileInfo(filename).baseName();
    m_id = "MeshRenderer:" + basename;
    init(mesh.first, mesh.second);
}

void MeshRenderer::init(const vector<MeshVertex>& vertices,
                        const vector<GLuint>& indices) {
    // Create Shader (Do not release until VAO is created)
    m_log->debug("BEGIN: Vertices size: {}, Indices size: {}",
               vertices.size(), indices.size());
    m_vertices.insert(m_vertices.end(), vertices.begin(), vertices.end());
    m_indices.insert(m_indices.end(), indices.begin(), indices.end());

    m_program = new QOpenGLShaderProgram();
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/simple.vert");
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/simple.frag");
    m_program->link();
    m_program->bind();
    float colorscheme[] = {0, 1, 1, 0, 1, 0, 1, 1, 0};
    m_colorScheme = QMatrix3x3(colorscheme);

    m_minColor = 1000000.0f, m_maxColor = -10000000.0f;
    m_center = QVector3D(0, 0, 0);
    for(size_t i = 0; i < m_vertices.size(); i++) {
        m_minColor = m_vertices[i].color() < m_minColor ? m_vertices[i].color() : m_minColor;
        m_maxColor = m_vertices[i].color() > m_maxColor ? m_vertices[i].color() : m_maxColor;
        m_center += m_vertices[i].position();
    }
    m_log->debug("Colorscale min: {} max: {}", m_minColor, m_maxColor);
    m_center /= m_vertices.size();

    // Create MeshVertex Buffer (Do not release until VAO is created)
    bool created = m_vertex.create();
    created = m_index.create();
    created = m_object.create();
    m_object.bind();

    m_vertex.bind();
    m_vertex.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_vertex.allocate(m_vertices.data(),
                      static_cast<int>(sizeof(MeshVertex) * m_vertices.size()));
    // Create Index Buffer (Do not release until VAO is created)

    m_index.bind();
    m_index.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_index.allocate(m_indices.data(),
                     static_cast<int>(sizeof(GLuint) * m_indices.size()));
    m_numberOfIndices = static_cast<GLsizei>(m_indices.size());

    m_program->enableAttributeArray(10);
    m_program->enableAttributeArray(11);
    m_program->enableAttributeArray(12);
    m_program->setAttributeBuffer(10, GL_FLOAT, MeshVertex::positionOffset(), MeshVertex::PositionTupleSize, MeshVertex::stride());
    m_program->setAttributeBuffer(11, GL_FLOAT, MeshVertex::normalOffset(), MeshVertex::NormalTupleSize, MeshVertex::stride());
    m_program->setAttributeBuffer(12, GL_FLOAT, MeshVertex::colorOffset(), MeshVertex::ColorTupleSize, MeshVertex::stride());

    m_program->setUniformValue("u_colorscheme", m_colorScheme);
    m_program->setUniformValue("u_minColorValue", m_minColor);
    m_program->setUniformValue("u_maxColorValue", m_maxColor);

    // Release (unbind) all
    m_index.release();
    m_vertex.release();
    m_object.release();
    m_program->release();
    m_log->debug("END: Vertices size: {}, Indices size: {}",
               vertices.size(), indices.size());
}

MeshRenderer::~MeshRenderer() {
    m_log->debug(ll("Destroying MeshRenderer: " + m_id));
}

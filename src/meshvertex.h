#pragma once
#include <QVector3D>

class MeshVertex
{
public:
  // Constructors
  Q_DECL_CONSTEXPR MeshVertex();
  Q_DECL_CONSTEXPR explicit MeshVertex(const QVector3D &position);
  Q_DECL_CONSTEXPR MeshVertex(const QVector3D &position, const QVector3D& normal, const float color);

  // Accessors / Mutators
  Q_DECL_CONSTEXPR const QVector3D& position() const;
  Q_DECL_CONSTEXPR const QVector3D& normal() const;
  Q_DECL_CONSTEXPR inline float color() const {return m_color;}
  void setPosition(const QVector3D& position);
  void setNormal(const QVector3D& normal);
  void setColor(const float color);

  // OpenGL Helpers
  static const int PositionTupleSize = 3;
  static const int NormalTupleSize = 3;
  static const int ColorTupleSize = 1;
  static Q_DECL_CONSTEXPR int positionOffset();
  static Q_DECL_CONSTEXPR int normalOffset();
  static Q_DECL_CONSTEXPR int colorOffset();
  static Q_DECL_CONSTEXPR int stride();

private:
  QVector3D m_position;
  QVector3D m_normal;
  float m_color;
};

/*******************************************************************************
 * Inline Implementation
 ******************************************************************************/

// Note: Q_MOVABLE_TYPE means it can be memcpy'd.
Q_DECLARE_TYPEINFO(MeshVertex, Q_MOVABLE_TYPE);

// Constructors
Q_DECL_CONSTEXPR inline MeshVertex::MeshVertex() : m_color(0) {}
Q_DECL_CONSTEXPR inline MeshVertex::MeshVertex(const QVector3D &position) : m_position(position), m_normal({0,0,0}), m_color(0) { }
Q_DECL_CONSTEXPR inline MeshVertex::MeshVertex(const QVector3D &position, const QVector3D& normal,
                                               const float color) : m_position(position), m_normal(normal), m_color(color) {}

// Accessors / Mutators
Q_DECL_CONSTEXPR inline const QVector3D& MeshVertex::position() const { return m_position; }
Q_DECL_CONSTEXPR inline const QVector3D& MeshVertex::normal() const { return m_normal; }
void inline MeshVertex::setPosition(const QVector3D& position) { m_position = position; }
void inline MeshVertex::setNormal(const QVector3D& normal) { m_normal = normal; }
void inline MeshVertex::setColor(const float color) { m_color = color; }

// OpenGL Helpers
Q_DECL_CONSTEXPR inline int MeshVertex::positionOffset() { return offsetof(MeshVertex, m_position); }
Q_DECL_CONSTEXPR inline int MeshVertex::normalOffset() { return offsetof(MeshVertex, m_normal); }
Q_DECL_CONSTEXPR inline int MeshVertex::colorOffset() { return offsetof(MeshVertex, m_color); }
Q_DECL_CONSTEXPR inline int MeshVertex::stride() { return sizeof(MeshVertex); }

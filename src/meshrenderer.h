#pragma once
#include "renderer.h"
#include "meshvertex.h"
#include "logging.h"
#include <vector>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>

using std::vector;

class MeshRenderer : public IndexedRenderer {
public:
    MeshRenderer(const vector<MeshVertex>& vertices,
                 const vector<GLuint>& indices);
    MeshRenderer(const QString& filename);
    virtual ~MeshRenderer();
    void init(const vector<MeshVertex>& vertices,
              const vector<GLuint>& indices);
private:
    QOpenGLBuffer m_vertex;
    vector<MeshVertex> m_vertices;
    vector<GLuint> m_indices;
    float m_minColor;
    float m_maxColor;
    QMatrix3x3 m_colorScheme;
    Logger m_log;
};

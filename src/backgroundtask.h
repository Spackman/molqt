#pragma once
#include <string>
#include <vector>
#include <set>
#include <QRunnable>
#include <QDir>
#include <QProcess>
#include <QTemporaryDir>
#include <QUuid>
#include "mustache.h"
#include "logging.h"

class TaskFile {
public:
    enum FileType { InputFile, OutputFile, WavefunctionFile, Miscellaneous };
    TaskFile() {}
    TaskFile(const TaskFile& other) : m_kind(other.m_kind), m_filename(other.m_filename), m_contents(other.m_contents) {}
    TaskFile(QString filename, FileType kind) : m_kind(kind), m_filename(filename) {}

    FileType kind() const { return m_kind; }
    void setKind(FileType kind) { m_kind = kind; }
    QString kindString() const {
        switch(m_kind) {
            case InputFile:
                return "I";
            case OutputFile:
                return "O";
            case WavefunctionFile:
                return "W";
            case Miscellaneous:
                return "M";
        }
    }
    QString filename() const { return m_filename; }
    QByteArray contents() const { return m_contents; }
    void setContents(QByteArray contents) { m_contents = contents; }

private:
    FileType m_kind = FileType::Miscellaneous;
    QString m_filename = "unknown_file";
    QByteArray m_contents = {};
};

Q_DECLARE_METATYPE(TaskFile);

using std::vector;
using std::set;


class BackgroundTask : public QObject, public QRunnable
{
    Q_OBJECT
public:
    enum Kind { TontoCIF, TontoBasic, G09Wavefunction };

    BackgroundTask(const Kind jobkind,
                   const vector<TaskFile>& inputs,
                   const vector<TaskFile>& outputs,
                   const QVariantHash& params = {},
                   const set<QUuid>& dependencies = {});
    void run();
    QString id() const { return m_id.toString().mid(1, 36).toUpper(); }
    std::string description() const { return m_description; }
    const vector<TaskFile>& inputs() { return m_inputs; }
    const vector<TaskFile>& outputs() { return m_outputs; }

    const QString executablePath() const {
        switch(m_kind) {
            case TontoCIF:
            /* fallthrough */
            case TontoBasic:
                return QDir::homePath() + "/repos/tonto/build/tonto";
            case G09Wavefunction:
                return "g09";
        }
    }

    const QString templateName() const {
        switch(m_kind) {
            case TontoCIF:
                return "cif.tonto";
            case TontoBasic:
                return "basic.tonto";
            case G09Wavefunction:
                return "wavefunction.g09";
        }
    }

    const QStringList executableParams() const {
        QStringList arguments = {};
        switch(m_kind) {
            case TontoCIF:
            /* fallthrough */
            case TontoBasic:
            {
                for(const auto& f: m_inputs) {
                    if(f.kind() == TaskFile::InputFile)
                        arguments << "-i" << f.filename().replace("%job_id", id());
                }
                for(const auto& f: m_outputs) {
                    if(f.kind() == TaskFile::OutputFile)
                        arguments << "-o" << f.filename().replace("%job_id", id());
                }
                break;
            }
            case G09Wavefunction:
            {
                for(const auto& f: m_inputs) {
                    if(f.kind() == TaskFile::InputFile)
                        arguments << f.filename().replace("%job_id", id());
                }
                break;
            }
        }
        return arguments;
    }

    static const QVariantHash defaultParams(const Kind kind) {
        switch(kind) {
        case TontoCIF:
            return QVariantHash({
                                    {"name", "job_%job_id"},
                                    {"cif", "input.cif"},
                                    {"cxc", "job_%job_id.cxc"}
                                });
        case TontoBasic:
            return QVariantHash({
                                    {"name", "job_%job_id"},
                                    {"basis_name", "STO-3G"},
                                    {"charge", QString::number(0)}, // Need to avoid QVariant(0) being written as blank
                                    {"atoms", "H 0.0 0.0 0.0\nH 1.4 0.0 0.0"},
                                    {"multiplicity", 1},
                                    {"wavefunction", "%job_id_wfn.sbf"}
                                });
        case G09Wavefunction:
            return QVariantHash({
                                    {"name", "job_%job_id"},
                                    {"method", "HF"},
                                    {"basis_name", "STO-3G"},
                                    {"charge", QString::number(0)}, // Need to avoid QVariant(0) being written as blank
                                    {"atoms", "H 0.0 0.0 0.0\nH 1.4 0.0 0.0"},
                                    {"multiplicity", 1}
                                });
        }
    }

    void setDescription(std::string description) {
        m_description = description;
    }

    bool dependsOn(const QUuid& id) const {
        auto search = m_dependencies.find(id);
        return search != m_dependencies.end();
    }

    const set<QUuid> dependencies() const { return m_dependencies; }

signals:
    void outputFileContents(const QString& id, const TaskFile& file);
    void finished(const QString& id);

private:
    Kind m_kind;
    QUuid m_id;
    QDir m_directory;
    QTemporaryDir m_workingDirectory;
    bool m_inWorkingDirectory = false;
    vector<TaskFile> m_inputs = {};
    vector<TaskFile> m_outputs = {};
    std::string m_description = "";
    QProcess * m_process;
    QVariantHash m_params;
    bool m_running = false;
    void renderInputs();
    void writeInputFiles();
    void readOutputs();
    void callProgram();
    set<QUuid> m_dependencies{};
    Logger m_log;
};

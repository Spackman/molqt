#include "scene.h"
#include "atom.h"
#include "meshrenderer.h"
#include "sphereimpostorrenderer.h"
#include "cylinderimpostorrenderer.h"
#include <chrono>


inline void Scene::setRendererUniforms(Renderer * renderer) {
    auto time = std::chrono::steady_clock::now().time_since_epoch();
    auto prog = renderer->program();
    prog->setUniformValue("u_view", m_camera.view());
    prog->setUniformValue("u_model", m_camera.model());
    prog->setUniformValue("u_proj", m_camera.projection());
    prog->setUniformValue("u_modelViewMat", m_camera.modelView());
    prog->setUniformValue("u_MVPMat", m_camera.modelViewProjection());
    prog->setUniformValue("u_normalMat", m_camera.normal());
    prog->setUniformValue("u_lightPos", m_camera.location()*1.02f);
    prog->setUniformValue("u_cameraPos", m_camera.location());
    prog->setUniformValue("u_time", std::chrono::duration<float>(time).count());
    m_log = getLogger("graphics");

}

Scene::Scene(QObject* parent) : QObject(parent),
    m_spheres({}), m_cylinders({}),
    m_tree()
{
    m_log = getLogger("graphics");
}

void Scene::resize(int width, int height)
{
    m_camera.onResize(width, height);
}

void Scene::draw() {
    // Render using our shader
    // Clear
    glClear(GL_COLOR_BUFFER_BIT);
    m_spheres.bind();
    setRendererUniforms(&m_spheres);
    m_spheres.draw();
    m_spheres.release();
    m_cylinders.bind();
    setRendererUniforms(&m_cylinders);
    m_cylinders.draw();
    m_cylinders.release();

    for(auto renderObject : m_renderObjects) {
        renderObject->bind();
        setRendererUniforms(renderObject);
        renderObject->draw();
        renderObject->release();
    }
}

void Scene::setProjectionType(const CameraProjection type)
{
    m_camera.setProjectionType(type);
}

CameraProjection Scene::projectionType() {
    return m_camera.projectionType();
}

void Scene::mouseScroll(const QPoint& delta)
{
    m_camera.onMouseScroll(delta.y());
}

void Scene::mouseDrag(const QPoint& delta)
{
    m_camera.onMouseDrag(delta);
}

void Scene::addRenderObject(Renderer * renderer) {
    m_log->debug(ll("Appending " + renderer->id() + " to renderObjects"));
    m_renderObjects.push_back(renderer);
}

QString Scene::info() {
    QString i = "Number of objects: %1";
    i = i.arg(m_renderObjects.size());
    return i;
}

void Scene::hit(const QVector3D& vnear, const QVector3D& vfar) {
    auto t1 = std::chrono::steady_clock::now();
    auto result = m_tree.hit(vnear, vfar);
    auto t2 = std::chrono::steady_clock::now();
    m_log->debug("Finding closest points took: {} ms",
                 std::chrono::duration<double, std::milli>(t2 - t1).count());
    m_log->debug("Hit @{} d = {}", result.index, result.proximity);
    if( (result.valid) && result.proximity < m_spheres.sphereRadius(result.index) + 0.1f)
    {
        toggleSelected({result.index});
    }
}

#include "templaterenderer.h"
#include <QFile>
#include <QTextStream>
#include <chrono>

TemplateRenderer::TemplateRenderer(QString templateName) :
    m_time(0), m_templateName(templateName)
{
    QFile f(":/templates/"+m_templateName);
    if (f.open(QFile::ReadOnly | QFile::Text))
    {
        QTextStream in(&f);
        m_templateText = QString(in.readAll());
    }
}

QString TemplateRenderer::render(const QVariantHash& context) const {
    auto t1 = std::chrono::steady_clock::now();
    Mustache::QtVariantContext ctx(context);
    QString result = m_renderer.render(m_templateText, &ctx);
    auto t2 = std::chrono::steady_clock::now();
    m_time = std::chrono::duration<double, std::milli>(t2 - t1).count();
    return result;
}


bool TemplateRenderer::renderToFile(const QVariantHash& context, QString filename) const
{
    QFile f(filename);
    if (f.open(QFile::WriteOnly | QFile::Text)) {
        f.write(render(context).toLocal8Bit());
        return true;
    }
    return false;
}

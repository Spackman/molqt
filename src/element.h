#pragma once
#include <QString>
#include <vector>
#include <QVector3D>

#define HYDROGEN   {1, "H", "hydrogen", 0.23f}
#define HELIUM     {2, "He", "helium", 0.31f}
#define LITHIUM    {3, "Li", "lithium", 1.67f}
#define BERYLLIUM  {4, "Be", "beryllium", 1.12f}
#define BORON      {5, "B", "boron", 0.87f}
#define CARBON     {6, "C", "carbon", 0.67f}
#define NITROGEN   {7, "N", "nitrogen", 0.56f}
#define OXYGEN     {8, "O", "oxygen", 0.48f}
#define FLUORINE   {9, "F", "fluorine", 0.42f}
#define NEON       {10, "Ne", "neon", 0.38f}
#define SODIUM     {11, "Na", "sodium", 1.9f}
#define MAGNESIUM  {12, "Mg", "magnesium", 1.45f}
#define ALUMINIUM  {13, "Al", "aluminium", 1.18f}
#define SILICON    {14, "Si", "silicon", 1.11f}
#define PHOSPHORUS {15, "P", "phosphorus", 0.98f}
#define SULFUR     {16, "S", "sulfur", 0.88f}
#define CHLORINE   {17, "Cl", "chlorine", 0.79f}
#define ARGON      {18, "Ar", "argon", 0.71f}
#define POTASSIUM  {19, "K", "potassium", 2.43f}
#define CALCIUM    {20, "Ca", "calcium", 1.94f}
#define SCANDIUM   {21, "Sc", "scandium", 1.84f}
#define TITANIUM   {22, "Ti", "titanium", 1.76f}
#define VANADIUM   {23, "V", "vanadium", 1.71f}
#define CHROMIUM   {24, "Cr", "chromium", 1.66f}
#define MANGANESE  {25, "Mn", "manganese", 1.61f}
#define IRON       {26, "Fe", "iron", 1.56f}
#define COBALT     {27, "Co", "cobalt", 1.52f}
#define NICKEL     {28, "Ni", "nickel", 1.49f}
#define COPPER     {29, "Cu", "copper", 1.45f}
#define ZINC       {30, "Zn", "zinc", 1.42f}
#define GALLIUM    {31, "Ga", "gallium", 1.36f}
#define GERMANIUM  {32, "Ge", "germanium", 1.25f}
#define ARSENIC    {33, "As", "arsenic", 1.14f}
#define SELENIUM   {34, "Se", "selenium", 1.03f}
#define BROMINE    {35, "Br", "bromine", 0.94f}
#define KRYPTON    {36, "Kr", "krypton", 0.88f}
#define RUBIDIUM   {37, "Rb", "rubidium", 2.65f}
#define STRONTIUM  {38, "Sr", "strontium", 2.19f}
#define YTTRIUM    {39, "Y", "yttrium", 2.12f}
#define ZIRCONIUM  {40, "Zr", "zirconium", 2.06f}
#define NIOBIUM    {41, "Nb", "niobium", 1.98f}
#define MOLYBDENUM {42, "Mo", "molybdenum", 1.9f}
#define TECHNETIUM {43, "Tc", "technetium", 1.83f}
#define RUTHENIUM  {44, "Ru", "ruthenium", 1.78f}
#define RHODIUM    {45, "Rh", "rhodium", 1.73f}
#define PALLADIUM  {46, "Pd", "palladium", 1.69f}
#define SILVER     {47, "Ag", "silver", 1.65f}
#define CADMIUM    {48, "Cd", "cadmium", 1.61f}
#define INDIUM     {49, "In", "indium", 1.56f}
#define TIN        {50, "Sn", "tin", 1.45f}
#define ANTIMONY   {51, "Sb", "antimony", 1.33f}
#define TELLURIUM  {52, "Te", "tellurium", 1.23f}
#define IODINE     {53, "I", "iodine", 1.15f}
#define XENON      {54, "Xe", "xenon", 1.08f}
#define CAESIUM    {55, "Cs", "caesium", 2.98f}

using std::vector;

struct Element {
    std::size_t atomic_number;
    QString symbol;
    QString name;
    float atomic_radius;
    Element(std::size_t n, QString s, QString f, float r) :
        atomic_number{n}, symbol{s}, name{f}, atomic_radius{r} {}

    static Element fromSymbol(const QString& symbol);
    QVector3D color() const;
};




/*
    SceneGraphItem.cpp

    A container for items of data supplied by the simple tree model.
*/

#include "scenegraphitem.h"

#include <QStringList>

//! [0]
SceneGraphItem::SceneGraphItem(const QVector<QVariant> &data, SceneGraphItem *parent)
{
    parentItem = parent;
    itemData = data;
}
//! [0]

//! [1]
SceneGraphItem::~SceneGraphItem()
{
    qDeleteAll(childItems);
}
//! [1]

//! [2]
SceneGraphItem *SceneGraphItem::child(int number)
{
    return childItems.value(number);
}
//! [2]

//! [3]
int SceneGraphItem::childCount() const
{
    return childItems.count();
}
//! [3]

//! [4]
int SceneGraphItem::childNumber() const
{
    if (parentItem)
        return parentItem->childItems.indexOf(const_cast<SceneGraphItem*>(this));

    return 0;
}
//! [4]

//! [5]
int SceneGraphItem::columnCount() const
{
    return itemData.count();
}
//! [5]

//! [6]
QVariant SceneGraphItem::data(int column) const
{
    return itemData.value(column);
}
//! [6]

//! [7]
bool SceneGraphItem::insertChildren(int position, int count, int columns)
{
    if (position < 0 || position > childItems.size())
        return false;

    for (int row = 0; row < count; ++row) {
        QVector<QVariant> data(columns);
        SceneGraphItem *item = new SceneGraphItem(data, this);
        childItems.insert(position, item);
    }

    return true;
}
//! [7]

//! [8]
bool SceneGraphItem::insertColumns(int position, int columns)
{
    if (position < 0 || position > itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        itemData.insert(position, QVariant());

    foreach (SceneGraphItem *child, childItems)
        child->insertColumns(position, columns);

    return true;
}
//! [8]

//! [9]
SceneGraphItem *SceneGraphItem::parent()
{
    return parentItem;
}
//! [9]

//! [10]
bool SceneGraphItem::removeChildren(int position, int count)
{
    if (position < 0 || position + count > childItems.size())
        return false;

    for (int row = 0; row < count; ++row)
        delete childItems.takeAt(position);

    return true;
}
//! [10]

bool SceneGraphItem::removeColumns(int position, int columns)
{
    if (position < 0 || position + columns > itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        itemData.remove(position);

    foreach (SceneGraphItem *child, childItems)
        child->removeColumns(position, columns);

    return true;
}

//! [11]
bool SceneGraphItem::setData(int column, const QVariant &value)
{
    if (column < 0 || column >= itemData.size())
        return false;

    itemData[column] = value;
    return true;
}
//! [11]

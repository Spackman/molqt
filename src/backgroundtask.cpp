#include "backgroundtask.h"
#include <QDateTime>
#include "templaterenderer.h"

BackgroundTask::BackgroundTask(const Kind kind,
                               const vector<TaskFile>& inputs,
                               const vector<TaskFile>& outputs,
                               const QVariantHash& params,
                               const set<QUuid>& dependencies)
        : m_kind(kind),
          m_id(QUuid::createUuid()),
          m_directory(QDir::current()),
          m_inputs(inputs),
          m_outputs(outputs),
          m_params(defaultParams(m_kind)),
          m_dependencies(dependencies)
{
    m_log = getLogger("tasks");
    for(const auto& key: params.keys()) {
        m_params[key] = params[key];
    }
    renderInputs();
}

void BackgroundTask::run()
{
    m_running = true;
#ifdef BENCHMARKING
    auto t1 = std::chrono::steady_clock::now();
#endif
    writeInputFiles();
    callProgram();
    readOutputs();
#ifdef BENCHMARKING
    auto t2 = std::chrono::steady_clock::now();
    m_log->info(ll(id() + "total took: {} ms"),
               std::chrono::duration<double, std::milli>(t2 - t1).count());
#endif
    m_running = false;
    emit finished(id());
}

void BackgroundTask::writeInputFiles() {
    for(const auto& x: m_inputs) {
        QString dst = m_workingDirectory.path() + QDir::separator() + x.filename().replace("%job_id", id());
        QFile f(dst);
        if (f.open(QFile::WriteOnly)) {
            f.write(x.contents());
        }
    }

}


void BackgroundTask::readOutputs() {
    for(auto& x: m_outputs) {
        QString src = m_workingDirectory.path() + QDir::separator() + x.filename().replace("%job_id", id());
        QFile f(src);
        if (f.open(QFile::ReadOnly))
        {
            x.setContents(f.readAll());
        }
        emit outputFileContents(id(), x);
    }
}

void BackgroundTask::callProgram() {
    m_process = new QProcess();
#ifdef BENCHMARKING
    auto t1 = std::chrono::steady_clock::now();
#endif
    m_process->setWorkingDirectory(m_workingDirectory.path());
    m_process->setEnvironment({
                                  "TONTO_BASIS_SET_DIRECTORY="+QDir::homePath() + "/repos/tonto/basis_sets",
                                  "GAUSS_EXEDIR="+QDir::homePath() + "/bin/g09",
                                  "G09BASIS="+QDir::homePath() + "/bin/g09/basis"
                              });

    m_process->start(executablePath(), executableParams());
    m_process->waitForFinished();
#ifdef BENCHMARKING
    auto t2 = std::chrono::steady_clock::now();
    m_log->info(ll(id() + "program took: {} ms"),
               std::chrono::duration<double, std::milli>(t2 - t1).count());

#endif
}

void BackgroundTask::renderInputs() {
    auto t = TemplateRenderer(templateName());
    m_params["name"] = m_params["name"].toString().replace("%job_id", id());
    m_params["wavefunction"] = m_params["wavefunction"].toString().replace("%job_id", id());
    for(auto& input : m_inputs) {
        input.setContents(t.render(m_params).toLocal8Bit());
    }
}

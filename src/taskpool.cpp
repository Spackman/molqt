#include "taskpool.h"

TaskPool::TaskPool(Datastore* datastore, int numTasks) :
    m_numTasks(numTasks),
    m_datastore(datastore),
    m_pool(QThreadPool::globalInstance())
{
    LOG = getLogger("tasks");
}

void TaskPool::add(BackgroundTask *task) {
    bool success = m_datastore->addJob(task->id(), "tonto wavefunction");
    for(const auto& input: task->inputs()) {
        success = m_datastore->addJobFile(task->id(),
                                          input);
    }

    QObject::connect(task,
                     &BackgroundTask::outputFileContents,
                     this,
                     &TaskPool::storeOutputContents);
    QObject::connect(task,
                     &BackgroundTask::finished,
                     this,
                     &TaskPool::taskFinished);
    LOG->debug(ll("Task " + task->id() + " added to pool"));
    m_pool->start(task);
    m_numTasksTotal = m_numTasksTotal + 1;

}

void TaskPool::storeOutputContents(const QString& id, const TaskFile& file)
{
    bool success = m_datastore->addJobFile(id, file);
    LOG->debug(ll("Task " + id +
                  " read '" + file.filename() +
                  "' output, stored in DB: {} "),
               success);
}

int TaskPool::activeJobs() const {
    return m_pool->activeThreadCount();
}

void TaskPool::resetTaskCounts() {
    if(m_numTasksCompleted >= m_numTasksTotal) {
        m_numTasksCompleted = 0;
        m_numTasksTotal = 0;
        LOG->debug("Resetting task counts");
    }
}

void TaskPool::taskFinished(const QString& id) {
    LOG->debug(ll("Task " + id + " finished"));
    m_numTasksCompleted = m_numTasksCompleted + 1;
    emit numActiveTasksChanged(m_numTasksCompleted, m_numTasksTotal);
    m_timer.singleShot(1000, this, SLOT(resetTaskCounts()));
}

void TaskPool::setDatastore(Datastore * datastore) {
    LOG->debug("Closing current datastore");
    delete m_datastore;
    m_datastore = datastore;
}

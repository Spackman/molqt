#pragma once
#include "logging.h"
#include "renderer.h"
#include "cylinderimpostorvertex.h"
#include <vector>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>

using std::vector;

class CylinderImpostorRenderer : public IndexedRenderer
{
public:
    static constexpr size_t MaxVertices = 65536 / sizeof(CylinderImpostorVertex);
    CylinderImpostorRenderer(const vector<CylinderImpostorVertex>& vertices);
    void addVertices(const vector<CylinderImpostorVertex>& vertices);
    virtual ~CylinderImpostorRenderer();
private:
    void updateBuffers();
    QOpenGLBuffer m_vertex;
    vector<CylinderImpostorVertex> m_vertices;
    vector<GLuint> m_indices;
    vector<GroupIndex<CylinderImpostorVertex>> m_groups;
protected:
    bool m_impostor = true;
    std::shared_ptr<spd::logger> LOG;
};

#pragma once
#include <QVector3D>
#include <QVector2D>
#include <QString>

class CylinderImpostorVertex
{
public:
    // Constructors
    Q_DECL_CONSTEXPR explicit CylinderImpostorVertex() : m_radius(0)
    {
             m_to = QVector3D(0, 0, 0);
             m_from = QVector3D(0, 0, 0);
             m_color = QVector3D(0, 0, 0);
    }
    Q_DECL_CONSTEXPR explicit CylinderImpostorVertex(const QVector3D& from,
                                                     const QVector3D& to,
                                                     const QVector3D &color,
                                                     const float radius);
    // Accessors / Mutators
    Q_DECL_CONSTEXPR const QVector3D& from() const;
    Q_DECL_CONSTEXPR const QVector3D& color() const;
    Q_DECL_CONSTEXPR const QVector3D& to() const;
    Q_DECL_CONSTEXPR float radius() const;

    void setFrom(const QVector3D& from);
    void setTo(const QVector3D& to);
    void setColor(const QVector3D& color);
    void setRadius(const float radius);

    // OpenGL Helpers
    static const int FromTupleSize = 3;
    static const int ToTupleSize = 3;
    static const int ColorTupleSize = 3;
    static const int RadiusSize = 1;

    static Q_DECL_CONSTEXPR int fromOffset();
    static Q_DECL_CONSTEXPR int radiusOffset();
    static Q_DECL_CONSTEXPR int colorOffset();
    static Q_DECL_CONSTEXPR int toOffset();

    operator QString() const {
        return QString("Cylinder");
    }
    static Q_DECL_CONSTEXPR int stride();

private:
    QVector3D m_from;
    QVector3D m_to;
    QVector3D m_color;
    float m_radius;
};

/*******************************************************************************
 * Inline Implementation
 ******************************************************************************/

// Note: Q_MOVABLE_TYPE means it can be memcpy'd.
Q_DECLARE_TYPEINFO(CylinderImpostorVertex, Q_MOVABLE_TYPE);

// Constructors
Q_DECL_CONSTEXPR inline CylinderImpostorVertex::CylinderImpostorVertex(const QVector3D &from,
                                                                       const QVector3D &to,
                                                                       const QVector3D &color,
                                                                       const float radius) :
    m_from(from), m_to(to),
    m_color(color), m_radius(radius) {}

// Accessors / Mutators
Q_DECL_CONSTEXPR inline const QVector3D& CylinderImpostorVertex::from() const { return m_from; }
Q_DECL_CONSTEXPR inline const QVector3D& CylinderImpostorVertex::to() const { return m_to; }
Q_DECL_CONSTEXPR inline const QVector3D& CylinderImpostorVertex::color() const { return m_color; }
Q_DECL_CONSTEXPR inline float CylinderImpostorVertex::radius() const { return m_radius; }

void inline CylinderImpostorVertex::setFrom(const QVector3D& from) { m_from = from; }
void inline CylinderImpostorVertex::setTo(const QVector3D& to) { m_to = to; }
void inline CylinderImpostorVertex::setColor(const QVector3D& color) { m_color = color; }
void inline CylinderImpostorVertex::setRadius(const float radius) { m_radius = radius; }


// OpenGL Helpers
Q_DECL_CONSTEXPR inline int CylinderImpostorVertex::fromOffset() { return offsetof(CylinderImpostorVertex, m_from); }
Q_DECL_CONSTEXPR inline int CylinderImpostorVertex::toOffset() { return offsetof(CylinderImpostorVertex, m_to); }
Q_DECL_CONSTEXPR inline int CylinderImpostorVertex::colorOffset() { return offsetof(CylinderImpostorVertex, m_color); }
Q_DECL_CONSTEXPR inline int CylinderImpostorVertex::radiusOffset() { return offsetof(CylinderImpostorVertex, m_radius); }

Q_DECL_CONSTEXPR inline int CylinderImpostorVertex::stride() { return sizeof(CylinderImpostorVertex); }

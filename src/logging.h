#pragma once
#include <QString>
#include <spdlog/spdlog.h>

namespace spd = spdlog;

typedef std::shared_ptr<spd::logger> Logger;

static const char* ll(const QString& s) {
    return s.toLocal8Bit().data();
}

static auto newLogger(const char* name) {
    return spd::stdout_color_mt(name);
}

static auto getLogger(const char* name) {
    auto logger = spd::get(name);
    if(logger == nullptr) 
        return  newLogger(name);
    return logger;
}

#pragma once

#include <QList>
#include <QVariant>
#include <QVector>

//! [0]
class SceneGraphItem
{
public:
    explicit SceneGraphItem(const QVector<QVariant> &data, SceneGraphItem *parent = 0);
    ~SceneGraphItem();

    SceneGraphItem *child(int number);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    bool insertChildren(int position, int count, int columns);
    bool insertColumns(int position, int columns);
    SceneGraphItem *parent();
    bool removeChildren(int position, int count);
    bool removeColumns(int position, int columns);
    int childNumber() const;
    bool setData(int column, const QVariant &value);

private:
    QList<SceneGraphItem*> childItems;
    QVector<QVariant> itemData;
    SceneGraphItem *parentItem;
};
//! [0]

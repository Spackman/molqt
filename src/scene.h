#pragma once
#include "renderer.h"
#include "sphereimpostorrenderer.h"
#include "cylinderimpostorrenderer.h"
#include "orbitcamera.h"
#include <QObject>
#include <vector>
#include <QTimer>
#include "logging.h"
#include "kdtree.h"

using std::vector;

class Scene : public QObject
{
    Q_OBJECT
    public:
        Scene(QObject* parent = 0);
    void addRenderObject(Renderer * renderer);
    void draw();

    void resize(int width, int height);
    CameraProjection projectionType();
    void setProjectionType(const CameraProjection type);
    void mouseScroll(const QPoint& delta);
    void mouseDrag(const QPoint& delta);

    inline void addSpheres(const vector<SphereImpostorVertex>& spheres) {
        m_spheres.addVertices(spheres);
        for(size_t i = 0; i < spheres.size(); i+=4) {
            m_tree.addPoint(spheres[i].position());
        }
        m_tree.rebuildIndex();
    }

    inline QMatrix4x4 viewMatrix() { return m_camera.view(); }
    inline QMatrix4x4 projectionMatrix() { return m_camera.projection(); }
    inline void setRayDirection(QVector3D dir) { m_rayDirection = dir; }
    inline QVector3D rayDirection() { return m_rayDirection; }

    inline void addCylinders(const vector<CylinderImpostorVertex>& cylinders) {
        m_cylinders.addVertices(cylinders);
    }

    inline void toggleSelected(const vector<size_t>& atomIndices) {
        m_spheres.toggleSelected(atomIndices);
    }
    void hit(const QVector3D&, const QVector3D&);
    QString info();

private:
    inline void setRendererUniforms(Renderer * obj);
    vector<Renderer*> m_renderObjects;
    SphereImpostorRenderer m_spheres;
    CylinderImpostorRenderer m_cylinders;
    OrbitCamera m_camera;
    QVector3D m_rayDirection;
    QVector3D m_rayNear;
    QVector3D m_rayFar;
    KDTree m_tree;
    Logger m_log;
};

Q_DECLARE_METATYPE(Scene*);

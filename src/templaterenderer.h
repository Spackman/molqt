#pragma once
#include <QString>
#include "mustache.h"

class TemplateRenderer
{
public:
    TemplateRenderer(QString templateName);
    QString render(const QVariantHash& context) const;
    bool renderToFile(const QVariantHash& context, QString filename) const;
    double timeTaken() { return m_time; }

private:
    mutable double m_time;
    mutable Mustache::Renderer m_renderer;
    QString m_templateName;
    QString m_templateText;
};

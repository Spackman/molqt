#include "kdtree.h"

KDTree::KDTree() : m_cloud({}),
    m_tree(3, m_cloud, KDTreeSingleIndexAdaptorParams(10 /* max leaf size */))
{
    m_log = getLogger("graphics");
    m_tree.buildIndex();
}

const QueryResult KDTree::query(const QVector3D& pt, const size_t numResults) {
    vector<size_t> resultIndices(numResults);
    vector<float> resultDistances(numResults);
    KNNResultSet<float> resultSet(numResults);
    resultSet.init(resultIndices.data(), resultDistances.data());
    // not sure how else to do this other than old-style cast
    // should be fine as QVector3D should be a POD type.
    m_tree.findNeighbors(resultSet, (float*)(&pt), SearchParams(10));
    return {resultIndices, resultDistances};
}

inline const ClosestPoint KDTree::closest(const QVector3D& pt) {
    auto r = query(pt, 1);
    return {r.indices[0], r.distances[0], true};
}

bool BoundingBox::intersect(const Ray &r) const
{
    Logger m_log = getLogger("graphics");
    float tmin, tmax, tymin, tymax, tzmin, tzmax;
    QVector3D point;
    tmin = (bounds[r.sign[0]].x() - r.orig.x()) * r.invdir.x();
    tmax = (bounds[1-r.sign[0]].x() - r.orig.x()) * r.invdir.x();
    tymin = (bounds[r.sign[1]].y() - r.orig.y()) * r.invdir.y();
    tymax = (bounds[1-r.sign[1]].y() - r.orig.y()) * r.invdir.y();

    if ((tmin > tymax) || (tymin > tmax))
        return false;
    point.setX(tmin);
    if (tymin > tmin)
        tmin = tymin;
    if (tymax < tmax)
        tmax = tymax;
    point.setY(tmin);

    tzmin = (bounds[r.sign[2]].z() - r.orig.z()) * r.invdir.z();
    tzmax = (bounds[1-r.sign[2]].z() - r.orig.z()) * r.invdir.z();

    if ((tmin > tzmax) || (tzmin > tmax))
        return false;
    if (tzmin > tmin)
        tmin = tzmin;
    if (tzmax < tmax)
        tmax = tzmax;
    point.setZ(tmin);
    m_log->debug("Intersection point: [{}, {}, {}]", point.x(), point.y(), point.z());
    return true;
}
/*
 * This is a pretty dodgy hack to raymarch.
 * Should definitely properly implement bounding (spheres) boxes
 * for atoms etc.
*/
ClosestPoint KDTree::hit(const QVector3D& near, const QVector3D& far,
                   float eps) {
    QVector3D c = near;
    QVector3D direction = (far - near).normalized();
    ClosestPoint pt;
    int maxiterations = 100;


    if(m_cloud.pts.size() > 0) {
        const auto bbox = m_tree.root_bbox;
        QVector3D lower, upper;
        int i = 0;
        for(const auto& interval : bbox) {
            lower[i] = interval.low - 1.0f; // allow 1 angstrom error
            upper[i] = interval.high + 1.0f;
            i++;
        }
        BoundingBox b(lower, upper);
        bool doesIntersect = b.intersect({c, direction});
        m_log->debug("Intersects: {}", doesIntersect);
        if(!doesIntersect) return pt;

        float minStepsize = 0.4f;
        do {
            pt = closest(c);
            float stepSize = pt.proximity/30;
            stepSize = stepSize > minStepsize ? stepSize : minStepsize;
            c = c + (direction * stepSize);
            i++;
            //max 10 iterations
        } while((pt.proximity > eps) && (i < 100));
    }
    return pt;
}

#pragma once
#include <QVector3D>
#include <vector>
#include <assert.h>
#include <nanoflann.hpp>
#include <limits>
#include "logging.h"

#ifndef MAXFLOAT
constexpr float MAXFLOAT = std::numeric_limits<float>::max();
#endif
using namespace nanoflann;
using std::vector;

class Ray
{
public:
    Ray(const QVector3D& orig, const QVector3D& dir) : orig(orig), dir(dir)
    {
        invdir =  {1/dir.x(), 1/dir.y(), 1/dir.z()};
        sign[0] = (invdir.x() < 0);
        sign[1] = (invdir.y() < 0);
        sign[2] = (invdir.z() < 0);
    }
    QVector3D orig, dir;       // ray orig and dir
    QVector3D invdir;
    int sign[3];
};

class BoundingBox
{
public:
    BoundingBox(const QVector3D& vmin, const QVector3D& vmax)
    {
        bounds[0] = vmin;
        bounds[1] = vmax;
    }
    bool intersect(const Ray&) const;
    QVector3D bounds[2];
};

struct PointCloud
{
    vector<QVector3D> pts = {};

    // return the number of data points
    inline size_t kdtree_get_point_count() const {
        return pts.size();
    }

    // Ifs/elses here should be optimized away at compile time
    inline float kdtree_get_pt(const size_t idx, int dim) const {
        if(dim == 0) return pts[idx].x();
        else if (dim == 1) return pts[idx].y();
        else return pts[idx].z();
    }

    inline float kdtree_distance(const float *q, const size_t idx, size_t size) const {
        assert(size == 3);
        const auto qv = QVector3D(q[0], q[1], q[2]);
        const auto p = pts[idx];
        // return square distance
        return (qv - p).lengthSquared();
    }
    // Indicates we don't want to perform our own bounding
    // box calculation. If we've calculated our own
    // should be set in bb (inout variable), along
    // with returning true.
    template<class BoundingBox>
    bool kdtree_get_bbox(BoundingBox& /*bb*/) const { return false; }
};

struct QueryResult {
    vector<size_t> indices;
    vector<float> distances;
};

struct ClosestPoint {
    size_t index = 0;
    float proximity = MAXFLOAT;
    bool valid = false;
};

class KDTree
{
    typedef KDTreeSingleIndexAdaptor<
        L2_Simple_Adaptor<float, PointCloud > ,
        PointCloud,
        3 // number of dimensions
        > KDTreeType;
public:
    KDTree();
    const QueryResult query(const QVector3D& pt, const size_t numResults);
    inline const ClosestPoint closest(const QVector3D& pt);
    inline void addPoint(const QVector3D& pt) { m_cloud.pts.push_back(pt); }
    inline void addPoints(const vector<QVector3D>& pts) {
        m_cloud.pts.insert(m_cloud.pts.end(), pts.begin(), pts.end());
    }
    inline size_t numPoints() { return m_cloud.pts.size(); }
    inline void rebuildIndex() { m_tree.buildIndex(); }
    ClosestPoint hit(const QVector3D&, const QVector3D&, float eps=0.23f);
private:
    PointCloud m_cloud;
    KDTreeType m_tree;
    Logger m_log;
};

#pragma once
#include <QThreadPool>
#include <QHash>
#include "backgroundtask.h"
#include "datastore.h"
#include <QTimer>
#include "logging.h"

class TaskPool : public QObject
{
    Q_OBJECT
public:
    TaskPool(Datastore* datastore, int numTasks = 4);
    void add(BackgroundTask* task); // todo dependencies, priority
    int activeJobs() const;
    void setDatastore(Datastore *datastore);
signals:
    void numActiveTasksChanged(int, int);
private slots:
    void storeOutputContents(const QString& id, const TaskFile& file);
    void resetTaskCounts();
    void taskFinished(const QString& id);
private:
    QTimer m_timer;
    int m_numTasks;
    int m_numTasksTotal = 0;
    int m_numTasksCompleted = 0;
    Datastore *m_datastore;
    QThreadPool *m_pool;
    std::shared_ptr<spd::logger> LOG;
};

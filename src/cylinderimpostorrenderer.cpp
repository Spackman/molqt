#include "cylinderimpostorrenderer.h"
#include <QOpenGLShaderProgram>

CylinderImpostorRenderer::CylinderImpostorRenderer(const vector<CylinderImpostorVertex>& vertices) :
    m_vertex(QOpenGLBuffer::VertexBuffer)
{
    // Create Shader (Do not release until VAO is created)
    LOG = spd::get("graphics");
    m_program = new QOpenGLShaderProgram();
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/cylinder_impostor.vert");
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/cylinder_impostor.frag");
    m_program->link();
    m_program->bind();

    // Create CylinderImpostorVertex Buffer (Do not release until VAO is created)
    m_vertex.create();
    m_vertex.bind();
    m_vertex.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_vertex.allocate(vertices.data(), static_cast<int>(sizeof(CylinderImpostorVertex) * vertices.size()));

    m_index.create();
    m_index.bind();

    m_index.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    addVertices(vertices);

    // Create CylinderImpostorVertex Array Object
    m_object.create();
    m_object.bind();
    m_program->enableAttributeArray(0);
    m_program->enableAttributeArray(1);
    m_program->enableAttributeArray(2);
    m_program->enableAttributeArray(3);
    m_program->setAttributeBuffer(0, GL_FLOAT, CylinderImpostorVertex::fromOffset(),
                                  CylinderImpostorVertex::FromTupleSize, CylinderImpostorVertex::stride());
    m_program->setAttributeBuffer(1, GL_FLOAT, CylinderImpostorVertex::toOffset(),
                                  CylinderImpostorVertex::ToTupleSize, CylinderImpostorVertex::stride());
    m_program->setAttributeBuffer(2, GL_FLOAT, CylinderImpostorVertex::colorOffset(),
                                  CylinderImpostorVertex::ColorTupleSize, CylinderImpostorVertex::stride());
    m_program->setAttributeBuffer(3, GL_FLOAT, CylinderImpostorVertex::radiusOffset(),
                                  CylinderImpostorVertex::RadiusSize, CylinderImpostorVertex::stride());
    // Release (unbind) all
    m_index.release();
    m_object.release();
    m_vertex.release();
    m_program->release();
    m_id = "CylinderImpostor-" + Renderer::generateId();
}

void CylinderImpostorRenderer::addVertices(const vector<CylinderImpostorVertex>& vertices) {
    auto old_n = m_vertices.size();
    m_vertices.insert(m_vertices.end(), vertices.begin(), vertices.end());

    LOG->debug("m_vertices.size() {} -> {} ", old_n, m_vertices.size());

    for(size_t i = old_n/4; i < (m_vertices.size())/4 ; i++ ) {
        GLuint idx = static_cast<GLuint>(4*i);
        m_indices.insert(m_indices.end(), {idx, idx + 1, idx + 2, idx + 2, idx + 3, idx});
    }
    LOG->debug("m_indices.size() {} -> {} ", m_numberOfIndices, m_indices.size());

    m_numberOfIndices = static_cast<GLsizei>(m_indices.size());
    updateBuffers();
}

void CylinderImpostorRenderer::updateBuffers() {
    LOG->debug("Updating buffers");
    m_vertex.bind();
    m_index.bind();

    LOG->debug("Before; m_vertex.size(): {}, m_index.size(): {} ",
               m_vertex.size(), m_index.size());

    m_vertex.allocate(m_vertices.data(), static_cast<int>(sizeof(CylinderImpostorVertex) * m_vertices.size()));
    /* NEED TO DEAL WITH MORE THAN 65536 size buffer */
    m_index.allocate(m_indices.data(), static_cast<int>(sizeof(GLuint) * m_indices.size()));

    LOG->debug("After; m_vertex.size(): {}, m_index.size(): {} ",
               m_vertex.size(), m_index.size());}

// Destructor
CylinderImpostorRenderer::~CylinderImpostorRenderer()
{
    LOG->debug(ll("Destroying renderer: " + m_id));
}

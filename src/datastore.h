#pragma once
#include <QSqlDatabase>
#include <QSqlQuery>
#include "backgroundtask.h"
#include "logging.h"

class Datastore
{
public:
    Datastore(const QString& path);
    ~Datastore();
    bool isOpen() const { return m_db.isOpen(); }
    bool addJobFile(const QString& job_id, const TaskFile& file);
    QByteArray retrieveJobFile(const QString& job_id, const QString& filename) const;
    bool addJob(const QString& job_id, const QString& kind);

private:
    bool createTables();
    bool startSession();
    QSqlDatabase m_db;
    std::shared_ptr<spd::logger> LOG;
};

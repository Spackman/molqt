#include "xyzparser.h"

vector<CylinderImpostorVertex> fromBondedAtoms(const Atom& a, const Atom& b) {
    QVector3D color_a = a.color();
    QVector3D color_b = b.color();

    QVector3D midpoint = 0.5 * (a.position() + b.position());
    color_a.setZ(color_a.z() == 0.0f ? -0.0001f : -color_a.z());
    color_b.setZ(color_b.z() == 0.0f ? -0.0001f : -color_b.z());
    const float radius = 0.23f * 0.5f;
    return {
        CylinderImpostorVertex(a.position(), midpoint, color_a, -radius),
        CylinderImpostorVertex(a.position(), midpoint, color_a, radius),
        CylinderImpostorVertex(a.position(), midpoint, a.color(), -radius),
        CylinderImpostorVertex(a.position(), midpoint, a.color(), radius),
        CylinderImpostorVertex(b.position(), midpoint, color_b, -radius),
        CylinderImpostorVertex(b.position(), midpoint, color_b, radius),
        CylinderImpostorVertex(b.position(), midpoint, b.color(), -radius),
        CylinderImpostorVertex(b.position(), midpoint, b.color(), radius)
    };
}


XYZParser::XYZParser(const QString &filename)
{
    m_filename = filename;
    m_parsed = false;
    m_bonds_guessed = true;
}


void XYZParser::parseAtoms() {
    if(m_parsed) return;
    QFile file(m_filename);

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);
    int line_number = 1;
    int n_atoms = 0;
    while (!in.atEnd())
    {
       QString line = in.readLine();
       if(line_number == 1) n_atoms = line.toInt();
       else if(line_number > 2) {
           QStringList tokens = line.split(QRegExp("\\s+"));
           Element e = Element::fromSymbol(tokens[0]);
           m_atoms.push_back(Atom({tokens[1].toFloat(), tokens[2].toFloat(), tokens[3].toFloat()}, e));
       }
       line_number++;
    }
    file.close();
    m_parsed = true;
}


void XYZParser::guessBonds() {
    for(std::size_t i = 0; i < m_atoms.size(); i++) {
        for(std::size_t j = i+1; j < m_atoms.size(); j++) {
            if(m_atoms[i].isBondedTo(m_atoms[j])) m_bonds.push_back({i,j});
        }
    }
    m_bonds_guessed = true;
}

Fragment XYZParser::getFragment() {
    parseAtoms();
    guessBonds();
    return Fragment(m_atoms, m_bonds);
}

QPair<vector<SphereImpostorVertex>, vector<CylinderImpostorVertex>> XYZParser::atomsAndBonds() {
    parseAtoms();
    guessBonds();
    vector<SphereImpostorVertex> atom_vertices;
    for(const Atom& a: m_atoms) {
        auto verts = a.asSphereImpostorVertices();
        atom_vertices.insert(atom_vertices.end(), verts.begin(), verts.end());
    }
    vector<CylinderImpostorVertex> bonds;
    for(auto bond: m_bonds) {
        auto verts = fromBondedAtoms(m_atoms[bond.from], m_atoms[bond.to]);
        bonds.insert(bonds.end(), verts.begin(), verts.end());
    }
    return {atom_vertices, bonds};
}

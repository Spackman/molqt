#pragma once
#include <Eigen/Core>
#include <Eigen/Dense>
#include <QtOpenGL>
#include <QFileInfo>
#include <iostream>
#include "meshvertex.h"
#include "sbf.hpp"
#include <vector>
#include "logging.h"

using std::vector;

inline bool fileExists(QString path) {
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    if (check_file.exists() && check_file.isFile()) {
        return true;
    } else {
        return false;
    }
}

typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> CM_MatrixXq;
typedef Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> CM_MatrixXi;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> RM_MatrixXq;
typedef Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajorBit> RM_MatrixXi;

template<typename T, int ordering>
Eigen::Map<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, ordering> > matrix_from_dataset(const sbf::Dataset& dset) {
    // assume column major
    auto shape = dset.get_shape_vector();
    return Eigen::Map<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, ordering> >(dset.data_as<T>(),
                         static_cast<int>(shape[0]),
            static_cast<int>(shape[1]));
}

const QString status_string(const sbf::File& f) {
    QString result = "";
    auto status = f.status();
#define CASE(c) case sbf::File::Status::c: result += #c; break
    switch(status) {
        CASE(Open);
        CASE(Closed);
        CASE(FailedClosing);
        CASE(FailedOpening);
        CASE(FailedReadingDatablocks);
        CASE(FailedReadingHeaders);
        default:
            result += "Unknown?";
    }
#undef CASE
    return result;
}

QPair<vector<MeshVertex>, vector<GLuint>> fromSBF(const QString& filename) {

    auto log = getLogger("files");
    const char * filename_raw = filename.toUtf8().constData();
    log->debug("Filename: {}", filename_raw);
    log->debug("Before reading file");
    sbf::File f(filename_raw, sbf::reading, true);
    log->debug("Finished reading file, status={}", status_string(f).toLocal8Bit().data());

    auto vertex_dset = f.get_dataset("vertices");
    const char * property_name = "d_norm";
    auto vertex_normal_dset = f.get_dataset("vertex normals");
    auto prop_dset = f.get_dataset(property_name);
    auto prop = prop_dset.data_as<sbf::sbf_double>();

    auto vertex_data = matrix_from_dataset<double, Eigen::ColMajor>(vertex_dset);
    auto vertex_normal_data = matrix_from_dataset<double, Eigen::ColMajor>(vertex_normal_dset);
    vector<MeshVertex> vertices(vertex_data.cols());
    /*
     * TODO restructure to use std::algorithm
     * back copy
     */
    for(int i = 0; i < vertex_data.cols(); i++)
        vertices[i] = MeshVertex({static_cast<float>(vertex_data(0, i)),
                                  static_cast<float>(vertex_data(1, i)),
                                  static_cast<float>(vertex_data(2, i))},
        {static_cast<float>(vertex_normal_data(0, i)),
         static_cast<float>(vertex_normal_data(1, i)),
         static_cast<float>(vertex_normal_data(2, i))},
                                 static_cast<float>(prop[i]));


    auto face_dset = f.get_dataset("faces");
    auto face_data = matrix_from_dataset<int, Eigen::ColMajor>(face_dset);
    vector<GLuint> indices(face_data.cols() * face_data.rows());
    for(int i = 0; i < face_data.cols(); i++) {
        indices[3*i] = face_data(0, i) - 1;
        indices[3*i+1] = face_data(1, i) - 1;
        indices[3*i+2] = face_data(2, i) - 1;
    }

    return {vertices, indices};

}

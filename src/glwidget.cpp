#include "glwidget.h"
#include <QDebug>
#include <QString>
#include <QCoreApplication>
#include <QOpenGLShaderProgram>
#include <QKeyEvent>
#include <QtOpenGL>


GLWidget::GLWidget(QWidget* parent): QOpenGLWidget(parent),
    m_leftButtonPressed(false),
    m_frameCount(0),
    m_debugLogger(Q_NULLPTR),
    m_fps(0)
{
    m_log = getLogger("graphics");
}

void GLWidget::initializeGL()
{
#define GL_DEBUG
#ifdef GL_DEBUG
    m_debugLogger = new QOpenGLDebugLogger(this);
    if (m_debugLogger->initialize())
    {
        qDebug() << "GL_DEBUG Debug Logger" << m_debugLogger << "\n";
        connect(m_debugLogger,
                &QOpenGLDebugLogger::messageLogged,
                this,
                &GLWidget::messageLogged);
        m_debugLogger->startLogging();
    }
#endif // GL_DEBUG
    // Initialize OpenGL Backend
    initializeOpenGLFunctions();
    connect(context(), SIGNAL(aboutToBeDestroyed()), this, SLOT(teardownGL()), Qt::DirectConnection);
    connect(this, SIGNAL(frameSwapped()), this, SLOT(update()));
    printVersionInformation();

    // Set global information
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    m_time.start();
    m_scene = new Scene();
}

void GLWidget::resizeGL(int width, int height)
{
    m_scene->resize(width, height);
}

void GLWidget::paintGL()
{
#ifdef BENCHMARKING
      QOpenGLTimerQuery timerQuery;

      // must be called after the context has been created e.g. initializeGL()
      timerQuery.create();

      // must be called within the paintGL() method:
      timerQuery.begin();
#endif
    m_scene->draw();

    m_frameCount++;
    if(m_frameCount % FRAME_UPDATE_INTERVAL == 0) emit fpsUpdate(m_fps);
    if( m_time.elapsed() > 1000) {
        m_fps = m_frameCount / (m_time.elapsed() / 1000.0f);
    }
#ifdef BENCHMARKING
    // call some OpenGL commands
    timerQuery.end();
    float time_ms = timerQuery.waitForResult() / 1000000.0;
    m_log->info("Frame time ms: {}", time_ms);
#endif
}

void GLWidget::teardownGL()
{
    delete m_scene;
}

void GLWidget::update()
{
    QOpenGLWidget::update();
}


void GLWidget::keyPressEvent( QKeyEvent* e )
{
    switch ( e->key() )
    {
    case Qt::Key_Escape:
        QCoreApplication::instance()->quit();
        break;
    case Qt::Key_D:
        m_log->info("FPS: {}");
        break;
    case Qt::Key_Q:
        QCoreApplication::instance()->quit();
        break;
    case Qt::Key_P:
        m_scene->setProjectionType(CameraProjection::Perspective);
        break;
    case Qt::Key_O:
        m_scene->setProjectionType(CameraProjection::Orthographic);
        break;
    default:
        QOpenGLWidget::keyPressEvent( e );
    }
}

void GLWidget::keyReleaseEvent( QKeyEvent* e )
{
    switch ( e->key() )
    {
    case Qt::Key_0:
        break;
    default:
        QOpenGLWidget::keyReleaseEvent( e );
    }
}

void GLWidget::mousePressEvent( QMouseEvent* e )
{
    if ( e->button() == Qt::LeftButton )
    {
        m_leftButtonPressed = true;
        m_pos = m_prevPos = e->pos();
    }
    // Ray cast for object selection
    QVector3D v_near = QVector3D(m_pos.x(), height() - m_pos.y(), 0.0f);
    QVector3D v_far = QVector3D(m_pos.x(), height() - m_pos.y(), 1.0f);
    v_near = v_near.unproject(m_scene->viewMatrix(), m_scene->projectionMatrix(), QRect(0, 0, width(), height()));
    v_far = v_far.unproject(m_scene->viewMatrix(), m_scene->projectionMatrix(), QRect(0, 0, width(), height()));
    m_scene->hit(v_near, v_far);

    QOpenGLWidget::mousePressEvent( e );
}

void GLWidget::mouseReleaseEvent( QMouseEvent* e )
{
    if ( e->button() == Qt::LeftButton )
        m_leftButtonPressed = false;
    QOpenGLWidget::mouseReleaseEvent( e );
}


void GLWidget::wheelEvent(QWheelEvent* e) {
    QPoint delta = e->angleDelta();
    m_scene->mouseScroll(delta);
    QOpenGLWidget::wheelEvent(e);
}

void GLWidget::mouseMoveEvent( QMouseEvent* e )
{
    if ( m_leftButtonPressed )
    {
        m_pos = e->pos();
        QPoint delta = m_prevPos - m_pos;
        QPointF deltaf(delta.x() / width(),
                       delta.y() / height());
        m_scene->mouseDrag(delta);
        m_prevPos = m_pos;
    }
    QOpenGLWidget::mouseMoveEvent( e );

}

/*******************************************************************************
 * Private Helpers
 ******************************************************************************/

void GLWidget::printVersionInformation()
{
    QString glType;
    QString glVersion;
    QString glProfile;

    // Get Version Information
    glType = (context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL";
    glVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));

    // Get Profile Information
#define CASE(c) case QSurfaceFormat::c: glProfile = #c; break
    switch (format().profile())
    {
    CASE(NoProfile);
    CASE(CoreProfile);
    CASE(CompatibilityProfile);
    }
#undef CASE

    m_log->info(ll("version = " + glType + glVersion + "(" + glProfile + ")"));
}

void GLWidget::messageLogged(const QOpenGLDebugMessage &msg)
{
    QString error = "source=";
    // Format based on source
#define CASE(c) case QOpenGLDebugMessage::c: error += #c; break
    switch (msg.source())
    {
    CASE(APISource);
    CASE(WindowSystemSource);
    CASE(ShaderCompilerSource);
    CASE(ThirdPartySource);
    CASE(ApplicationSource);
    CASE(OtherSource);
    CASE(InvalidSource);
    CASE(AnySource);
    }
#undef CASE

    error += ", type=";

    // Format based on type
#define CASE(c) case QOpenGLDebugMessage::c: error += #c; break
    switch (msg.type())
    {
    CASE(ErrorType);
    CASE(DeprecatedBehaviorType);
    CASE(UndefinedBehaviorType);
    CASE(PortabilityType);
    CASE(PerformanceType);
    CASE(OtherType);
    CASE(MarkerType);
    CASE(GroupPushType);
    CASE(GroupPopType);
    CASE(AnyType);
    CASE(InvalidType);
    }
#undef CASE

    // Format based on severity
    QString message = "msg="+ msg.message();
    switch (msg.severity())
    {
    case QOpenGLDebugMessage::NotificationSeverity:
        m_log->info(ll(error));
        m_log->info(ll(message));
        break;
    case QOpenGLDebugMessage::HighSeverity:
        m_log->error(ll(error));
        m_log->error(ll(message));
        break;
    case QOpenGLDebugMessage::MediumSeverity:
        m_log->error(ll(error));
        m_log->error(ll(message));
        break;
    case QOpenGLDebugMessage::LowSeverity:
        m_log->warn(ll(error));
        m_log->warn(ll(message));
        break;
    case QOpenGLDebugMessage::AnySeverity:
        m_log->info(ll(error));
        m_log->info(ll(message));
        break;
    case QOpenGLDebugMessage::InvalidSeverity:
        m_log->error(ll(error));
        m_log->error(ll(message));
        break;
    }

    //QApplication::exit(1);
}

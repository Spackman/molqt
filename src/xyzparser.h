#pragma once
#include <QFile>
#include "cylinderimpostorrenderer.h"
#include "sphereimpostorrenderer.h"
#include "atom.h"

class XYZParser
{
public:
    XYZParser(const QString& filename);
    QPair<vector<SphereImpostorVertex>, vector<CylinderImpostorVertex>> atomsAndBonds();
    Fragment getFragment();
private:
    void parseAtoms();
    void guessBonds();
    QString m_filename;
    vector<Atom> m_atoms;
    vector<Bond> m_bonds;
    bool m_parsed;
    bool m_bonds_guessed;
};

#pragma once
#include <QVector3D>
#include <QVector2D>
#include <vector>
using std::vector;

class SphereImpostorVertex
{
public:
  // Constructors
  Q_DECL_CONSTEXPR explicit SphereImpostorVertex() : m_radius(0)
{        m_position = QVector3D(0, 0, 0);
         m_color = QVector3D(0, 0, 0);
         m_texcoord = QVector2D(0, 0);
}
  Q_DECL_CONSTEXPR explicit SphereImpostorVertex(const QVector3D &position, const QVector3D &color,
                                                 const float radius, const QVector2D &texcoord);

  // Accessors / Mutators
  Q_DECL_CONSTEXPR const QVector3D& position() const;
  Q_DECL_CONSTEXPR const QVector3D& color() const;
  Q_DECL_CONSTEXPR const QVector2D& texcoord() const;
  Q_DECL_CONSTEXPR float radius() const;
  void setPosition(const QVector3D& position);
  void setColor(const QVector3D& color);
  void setRadius(const float radius);
  void setTexcoord(const QVector2D& texcoord);

  // OpenGL Helpers
  static const int PositionTupleSize = 3;
  static const int ColorTupleSize = 3;
  static const int RadiusSize = 1;
  static const int TexcoordTupleSize = 2;
  static Q_DECL_CONSTEXPR int positionOffset();
  static Q_DECL_CONSTEXPR int radiusOffset();
  static Q_DECL_CONSTEXPR int colorOffset();
  static Q_DECL_CONSTEXPR int texcoordOffset();
  operator QString() const {
      return QString("[%1 %2 %3], [%4 %5 %6], %7,[%8 %9]").
              arg(m_position[0]).
              arg(m_position[1]).
              arg(m_position[2]).
              arg(m_color[0]).
              arg(m_color[1]).
              arg(m_color[2]).
              arg(m_radius).
              arg(m_texcoord[0])
              .arg(m_texcoord[1]);
  }
  static Q_DECL_CONSTEXPR int stride();

private:
  QVector3D m_position;
  QVector3D m_color;
  float m_radius;
  QVector2D m_texcoord;
};

/*******************************************************************************
 * Inline Implementation
 ******************************************************************************/

// Note: Q_MOVABLE_TYPE means it can be memcpy'd.
Q_DECLARE_TYPEINFO(SphereImpostorVertex, Q_MOVABLE_TYPE);

// Constructors
Q_DECL_CONSTEXPR inline SphereImpostorVertex::SphereImpostorVertex(const QVector3D &position, const QVector3D &color,
                                       const float radius, const QVector2D &texcoord) :
    m_position(position), m_color(color), m_radius(radius), m_texcoord(texcoord) {}

// Accessors / Mutators
Q_DECL_CONSTEXPR inline const QVector3D& SphereImpostorVertex::position() const { return m_position; }
Q_DECL_CONSTEXPR inline const QVector3D& SphereImpostorVertex::color() const { return m_color; }
Q_DECL_CONSTEXPR inline float SphereImpostorVertex::radius() const { return m_radius; }
Q_DECL_CONSTEXPR inline const QVector2D& SphereImpostorVertex::texcoord() const { return m_texcoord; }
void inline SphereImpostorVertex::setPosition(const QVector3D& position) { m_position = position; }
void inline SphereImpostorVertex::setColor(const QVector3D& color) { m_color = color; }
void inline SphereImpostorVertex::setRadius(const float radius) { m_radius = radius; }
void inline SphereImpostorVertex::setTexcoord(const QVector2D& texcoord) { m_texcoord = texcoord; }

// OpenGL Helpers
Q_DECL_CONSTEXPR inline int SphereImpostorVertex::positionOffset() { return offsetof(SphereImpostorVertex, m_position); }
Q_DECL_CONSTEXPR inline int SphereImpostorVertex::colorOffset() { return offsetof(SphereImpostorVertex, m_color); }
Q_DECL_CONSTEXPR inline int SphereImpostorVertex::radiusOffset() { return offsetof(SphereImpostorVertex, m_radius); }
Q_DECL_CONSTEXPR inline int SphereImpostorVertex::texcoordOffset() { return offsetof(SphereImpostorVertex, m_texcoord); }
Q_DECL_CONSTEXPR inline int SphereImpostorVertex::stride() { return sizeof(SphereImpostorVertex); }

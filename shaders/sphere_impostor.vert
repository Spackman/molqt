#version 330
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 color;
layout(location = 2) in float radius;
layout(location = 3) in vec2 texcoord;

uniform mat4 u_modelViewMat;
uniform mat4 u_proj;
out vec4 v_color;
out float v_radius;
out vec2 v_texcoord;
out vec4 v_eye_position;
out vec3 v_light_direction;
out vec2 mapping;
out float v_selected;

const vec3 lightPosition = vec3(0, 0, 2);
const float boxCorrection = 1.5;

void main()
{

  v_radius = radius;
  v_texcoord = texcoord;
  v_light_direction = normalize(lightPosition);
  v_eye_position = u_modelViewMat * vec4(position, 1.0);
  gl_Position = v_eye_position; // also referred to as cameraSpherePos
  gl_Position.xy += texcoord * radius * boxCorrection;
  mapping = texcoord * boxCorrection;
  gl_Position = u_proj *  gl_Position;
  v_selected = color.x < 0 ? 1.0 : 0.0;
  v_color = vec4(abs(color), 1.0);
}

#version 330
layout(location = 0) in highp vec3 from;
layout(location = 1) in highp vec3 to;
layout(location = 2) in highp vec3 color;
layout(location = 3) in highp float radius;

uniform mat4 u_model;
uniform mat4 u_proj;
uniform mat4 u_view;
uniform mat4 u_modelViewMat;
uniform mat3 u_normalMat;
uniform vec3 u_lightPos;

flat out highp vec3 vColor;
out highp vec3 vLight;
out highp vec3 cposition;
out highp vec3 p1;
out highp vec3 p2;
out highp float r;
const vec3 lightPosition = vec3(0, 0, 2);
const vec3 directionalLightColor = vec3(1,1,1);
const float boxCorrection = 1.5;

void main() {

    vColor = color; vColor.z = abs(vColor.z); //z indicates which vertex and so would vary
    r = abs(radius);
    vec4 cyl_to = u_modelViewMat * vec4(to, 1.0); //normal is other point of cylinder
    vec4 cyl_from = u_modelViewMat * vec4(from, 1.0);
    vec4 mvPosition = cyl_from;
    p1 = cyl_from.xyz; p2 = cyl_to.xyz;
    vec3 norm = cyl_to.xyz - cyl_from.xyz;
    float mult = boxCorrection; //slop to account for perspective of sphere

    if(length(p1) > length(p2)) { //billboard at level of closest point
       mvPosition = cyl_to;
    }
    vec3 n = normalize(mvPosition.xyz);
//intersect with the plane defined by the camera looking at the billboard point
    if(color.z >= 0.0) { //p1
       vec3 pnorm = normalize(p1);
       float t = dot(mvPosition.xyz-p1,n)/dot(pnorm,n);
       mvPosition.xyz = p1+t*pnorm;
    } else {
       vec3 pnorm = normalize(p2);
       float t = dot(mvPosition.xyz-p2,n)/dot(pnorm,n);
       mvPosition.xyz = p2+t*pnorm;
       mult *= -1.0;
    }
    vec3 cr = normalize(cross(mvPosition.xyz,norm))*radius;
    vec3 doublecr = normalize(cross(mvPosition.xyz,cr))*radius;
    mvPosition.xy +=  mult*(cr + doublecr).xy;
    cposition = mvPosition.xyz;
    gl_Position = u_proj * mvPosition;
    vec4 lDirection = normalize(vec4(lightPosition,0.0));
    vLight = normalize( lDirection.xyz )*directionalLightColor; //not really sure this is right, but color is always white so..
}

#pragma once
#include <QObject>

#include "datastore.h"
#include "taskpool.h"
#include "backgroundtask.h"

class BackgroundTaskTests : public QObject
{
    Q_OBJECT
public:
    BackgroundTaskTests();
    ~BackgroundTaskTests() override;

private:
    QString m_localPath;
    Datastore *m_datastore;
    TaskPool *m_taskpool;
    Logger m_log;

private Q_SLOTS:
    void initTestCase();

    void connectAndCreateDatabaseTest();
    void createTaskPoolTest();

    void cleanupTestCase();
};

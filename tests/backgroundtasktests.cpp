#include "backgroundtasktests.h"

#include <QDir>
#include <QFileInfo>
#include <QDateTime>
#include <QtTest/QtTest>

constexpr int numthreads = 4;

BackgroundTaskTests::BackgroundTaskTests()
    : QObject(),
      m_localPath("backgroundtasktests.sqlite3")
{
    m_log = newLogger("test");
}

BackgroundTaskTests::~BackgroundTaskTests()
{
}

void BackgroundTaskTests::initTestCase()
{
    QFileInfo file(m_localPath);
    if (file.exists()) {
        m_log->info("Test database file exists, deleting");
        QDir dir(file.absoluteDir());
        QVERIFY(dir.remove(file.fileName()));
    }
}

void BackgroundTaskTests::connectAndCreateDatabaseTest()
{
    m_datastore = new Datastore(m_localPath);
    bool result = m_datastore->isOpen();
    QVERIFY(result);
}

void BackgroundTaskTests::createTaskPoolTest()
{
    m_taskpool = new TaskPool(m_datastore, numthreads);
    QVERIFY(m_taskpool->activeJobs() == 0);
}


void BackgroundTaskTests::cleanupTestCase()
{
    delete m_taskpool;
    delete m_datastore;
    if (QDir::home().exists(m_localPath)) {
        bool result = QDir::home().remove(m_localPath);
        QVERIFY(result);
    }
}

QTEST_MAIN(BackgroundTaskTests)

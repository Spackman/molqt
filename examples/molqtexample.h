#pragma once
#include "ui_molqtexample.h"
#include "glwidget.h"
#include "taskpool.h"
#include "datastore.h"
#include "logging.h"
#include <QMainWindow>
#include <QProgressBar>

class QAction;
class QActionGroup;
class QLabel;
class QMenu;

namespace Ui {
class MolQTExample;
}

class MolQTExample : public QMainWindow
{
    Q_OBJECT

public:

    explicit MolQTExample(QWidget *parent = 0);
    ~MolQTExample();

private slots:
    void openFile();
    void quit();
    void saveSceneToFile();
    void fpsUpdate(float);
    void updateActions();
    void toggleProjection();
    void calculateWavefunction();
    void updateTaskProgress(int current, int total);

private:
    void createActions();
    void createMenus();
    void openFile(const QString& filename);
    void saveSceneToFile(const QString& filename);
    /* Actions */
    QAction *openFileAction;
    QAction *quitAction;
    QAction *saveSceneAction;
    QAction *toggleProjectionAction;
    QAction *wavefunctionAction;
    QAction *removeRowAction;
    QAction *removeColumnAction;
    /* Members */
    Ui::MolQTExample *ui;
    QMenu *fileMenu;
    QProgressBar *m_taskProgressBar;
    TaskPool *m_taskpool;
    Datastore *m_datastore;
    Logger m_log = nullptr;

protected:
    GLWidget *glWidget;
};

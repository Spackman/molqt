#include <QApplication>
#include "glwidget.h"
#include "molqtexample.h"
#include "backgroundtask.h"
#include "logging.h"

int main(int argc, char *argv[])
{
    spd::set_level(spd::level::debug); //Set global log level to info
    auto console = newLogger("main");
    auto console2 = newLogger("tasks");
    auto sbf_logger = newLogger("files");
    auto opengl_logger = newLogger("graphics");
    console->debug("Start up...");

    QApplication app(argc, argv);
    qsrand(QTime::currentTime().msec());
    // Need to register metatypes which will be passed
    // by signals/slots
    qRegisterMetaType<TaskFile>("TaskFile");

    MolQTExample * molqt = new MolQTExample();

    // Set the window up
    molqt->show();
    return app.exec();
}

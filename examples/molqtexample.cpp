#include "molqtexample.h"
#include "xyzparser.h"
#include "meshrenderer.h"
#include "ui_molqtexample.h"
#include "backgroundtask.h"
#include "datastore.h"
#include <QStandardItemModel>
#include <QStandardPaths>
#include <QProgressBar>
#include <QtWidgets>

MolQTExample::MolQTExample(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MolQTExample)
{
    m_log = getLogger("main");
    // Set OpenGL Version information
    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);
    format.setSamples(4);
    format.setVersion(3,3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setOption(QSurfaceFormat::DebugContext);
    QSurfaceFormat::setDefaultFormat(format);
    ui->setupUi(this);
    glWidget = new GLWidget(this);

    QTableWidget * table = new QTableWidget();
    this->ui->tabWidget->addTab(glWidget, QString("view"));
    this->ui->tabWidget->addTab(table, QString("info"));
    connect(glWidget, &GLWidget::fpsUpdate,
            this, &MolQTExample::fpsUpdate);
    QStringList headers;
    headers << tr("Name") << tr("Kind");
    QStandardItemModel * model = new QStandardItemModel;
    QStandardItem* rootNode = model->invisibleRootItem();
    QStandardItem* sceneNode = new QStandardItem("Scene");
    rootNode->appendRow(sceneNode);
    this->ui->treeView->setModel(model);
    connect(this->ui->treeView->selectionModel(),
            &QItemSelectionModel::selectionChanged,
            this,
            &MolQTExample::updateActions);

    m_taskProgressBar = new QProgressBar();
    m_taskProgressBar->setRange(0, 1);
    statusBar()->addPermanentWidget(m_taskProgressBar);
    createActions();
    createMenus();
    m_datastore = new Datastore("datastore.sqlite3");
    m_taskpool = new TaskPool(m_datastore, 4);
    connect(m_taskpool,
            &TaskPool::numActiveTasksChanged,
            this,
            &MolQTExample::updateTaskProgress);
}

MolQTExample::~MolQTExample()
{
    delete ui;
}

void MolQTExample::createMenus() {
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(openFileAction);
    fileMenu->addAction(quitAction);
    fileMenu->addAction(saveSceneAction);
    fileMenu->addAction(toggleProjectionAction);
    fileMenu->addAction(wavefunctionAction);
}

void MolQTExample::createActions() {
    openFileAction = new QAction(tr("&Open"), this);
    openFileAction->setShortcuts(QKeySequence::Open);
    openFileAction->setStatusTip(tr("Open a file"));
    connect(openFileAction,
            SIGNAL(triggered()),
            this,
            SLOT(openFile()));
    quitAction = new QAction(tr("&Quit"), this);
    quitAction->setShortcuts(QKeySequence::Quit);
    quitAction->setStatusTip("Exit the program");
    connect(quitAction,
            &QAction::triggered,
            this,
            &MolQTExample::quit);
    saveSceneAction = new QAction(tr("&Save as PNG"), this);
    saveSceneAction->setShortcut(QKeySequence::SaveAs);
    connect(saveSceneAction,
            SIGNAL(triggered()),
            this,
            SLOT(saveSceneToFile()));
    removeRowAction = new QAction(tr("Remove row"));
    removeColumnAction = new QAction(tr("Remove row"));
    toggleProjectionAction = new QAction(tr("&Toggle projection"), this);
    toggleProjectionAction->setShortcut(QKeySequence::Print);
    connect(toggleProjectionAction,
            SIGNAL(triggered()),
            this,
            SLOT(toggleProjection()));
    wavefunctionAction = new QAction(tr("&Calculate Wavefunction"), this);
    wavefunctionAction->setShortcuts(QKeySequence::Copy);
    wavefunctionAction->setStatusTip(
                tr("Calculate wavefunction for current molecule"));
    connect(wavefunctionAction,
            &QAction::triggered,
            this,
            &MolQTExample::calculateWavefunction);
}

void MolQTExample::fpsUpdate(float fps) {
    this->ui->statusBar->showMessage(
                QString().sprintf("%.1f fps", static_cast<double>(fps)));
}

void MolQTExample::openFile() {
    const QString filter = "SBF surface file (*.sbf *.cxs *.xyz)";
    QStringList filenames =
            QFileDialog::getOpenFileNames(
                this,
                tr("Open File"),
                QString(QStandardPaths::DesktopLocation),
                filter,
                Q_NULLPTR,
            #ifdef __linux
                QFileDialog::DontUseNativeDialog
            #else
                Q_NULLPTR
            #endif
                );
    if(filenames.size() > 0) {
        for(auto filename : filenames)
            openFile(filename);
    }
}

void MolQTExample::saveSceneToFile() {
    saveSceneToFile(QString("/Users/prs/test.png"));
}

void MolQTExample::openFile(const QString& filename) {
    m_log->info(ll("Opening: " + filename));
    if(filename.endsWith(".xyz")) {
        XYZParser parser(filename);
        auto molecule = parser.getFragment();
        if (molecule.bondCylinders().size() > 0)
            glWidget->scene()->addCylinders(molecule.bondCylinders());
        if (molecule.atomSpheres().size() > 0)
            glWidget->scene()->addSpheres(molecule.atomSpheres());
    }
    else {
        glWidget->scene()->addRenderObject(new MeshRenderer(filename));
    }
}


void MolQTExample::saveSceneToFile(const QString& filename) {

    QImage image = this->glWidget->grabFramebuffer();
    image.save(filename, "PNG");
}


void MolQTExample::calculateWavefunction() {
    auto bg = new BackgroundTask(
                BackgroundTask::Kind::TontoBasic,
    {
                    TaskFile("%job_id.inp", TaskFile::InputFile)
                },
    {
                    TaskFile("%job_id.log", TaskFile::OutputFile),
                    TaskFile("%job_id-wfn.sbf", TaskFile::WavefunctionFile)
                }
                );

    m_taskpool->add(bg);
}

void MolQTExample::updateActions()
{
    bool hasSelection =
            !this->ui->treeView->selectionModel()->selection().isEmpty();
    removeRowAction->setEnabled(hasSelection);
    removeColumnAction->setEnabled(hasSelection);

    bool hasCurrent =
            this->ui->treeView->selectionModel()->currentIndex().isValid();
    //    insertRowAction->setEnabled(hasCurrent);
    //    insertColumnAction->setEnabled(hasCurrent);

    if (hasCurrent) {
        auto currentIndex =
                this->ui->treeView->selectionModel()->currentIndex();
        this->ui->treeView->closePersistentEditor(currentIndex);

        int row = currentIndex.row();
        int column = currentIndex.column();
        if (currentIndex.parent().isValid())
            statusBar()->showMessage(
                        tr("Position: (%1,%2)").arg(row).arg(column));
        else
            statusBar()->showMessage(
                        tr("Position: (%1,%2) -- top").arg(row).arg(column));
    }
}

void MolQTExample::updateTaskProgress(int current, int total) {
    m_taskProgressBar->setMaximum(total);
    m_taskProgressBar->setValue(current);

}

void MolQTExample::quit() {
    QApplication::instance()->quit();
}


void MolQTExample::toggleProjection() {
    auto proj = glWidget->scene()->projectionType();

    if(proj == CameraProjection::Perspective) {
        proj = CameraProjection::Orthographic;
    }
    else {
        proj = CameraProjection::Perspective;
    }
    glWidget->scene()->setProjectionType(proj);
}
